# Braintree test integration

## How to run
- `npm install`
- `npm start`
- open `http://localhost:3000/` in your browser

## App
application build on top of Express generator for Node.js
[https://www.npmjs.com/package/express-generator](https://www.npmjs.com/package/express-generator)

server: node.js

client: js, jQuery

`public/javascripts/braintree.js` - wrapper above Braintree JS SDK v3. Requiers jQuery.
