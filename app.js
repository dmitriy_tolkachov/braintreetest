var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//braintree
// https://developers.braintreepayments.com/start/hello-server/node
// https://developers.braintreepayments.com/reference/general/testing/node
// https://sandbox.braintreegateway.com/merchants/yzw4sgmxx2gswstn/home
var braintree = require("braintree");
var config = require('./app.config.json');
var gateway = braintree.connect({
	environment: braintree.Environment.Sandbox,
	merchantId: config.braintree.merchantId,
	publicKey: config.braintree.publicKey,
	privateKey: config.braintree.privateKey
});

var routes = require('./routes/index');
//var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
//app.use('/users', users);

//braintree routes
app.get("/api/client_token", function (req, res) {
	gateway.clientToken.generate({}, function (err, response) {
		res.send(response.clientToken);
	});
});
app.post("/api/checkout", function (req, res) {
	//TODO: validate data
	var nonceFromTheClient = req.body.payment_method_nonce;
	var amount = req.body.amount;

	gateway.transaction.sale({
		amount: amount,
		paymentMethodNonce: nonceFromTheClient,
		options: {
			submitForSettlement: true
		}
	}, function (err, result) {
		if (err) {
			res.send(result);
		} else {
			res.send(result);
		}
	});
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});


module.exports = app;
