(function () {
	// https://developers.braintreepayments.com/start/hello-client/javascript/v3

	var form = document.querySelector('#checkout-form');
	var submit = document.querySelector('input[type="submit"]');

	var cardNumberSelector = '#card-number';
	var cvvSelector = '#cvv';
	var expDateSelector = '#expiration-date';

	JrBraintreeClient
		.init(cardNumberSelector, cvvSelector, expDateSelector)
		.done(onInitComplete)
		.fail(onError);

	function onInitComplete() {
		submit.removeAttribute('disabled');

		form.addEventListener('submit', function (event) {
			event.preventDefault();

			JrBraintreeClient
				.getPaymentMethodNonce()
				.done(function onOk(paymentMethodNonce) {
					JrBraintreeClient
						.checkout("10.00", paymentMethodNonce)
						.done(function onOk(res) {

							console.log(res);
							var msg = [
								'You payed ',
								res.transaction.amount,
								' ',
								res.transaction.currencyIsoCode,
								' from ',
								res.transaction.creditCard.maskedNumber
							].join('');
							alert(msg);

						})
						.fail(onError);
				})
				.fail(onError)
		}, false);
	}
	function onError(err) {
		console.log(err);
		alert('Error happened during Braintree initialization');
	}
})();