/**
 * https://developers.braintreepayments.com/start/hello-client/javascript/v3
 * architecture inspiration https://medium.freecodecamp.com/javascript-modules-a-beginner-s-guide-783f7d7a5fcc#.dwp2y93t3
 */
var JrBraintreeClient = (function () {
    //private properties
    var API = {
        CLIENT_TOKEN: '/api/client_token',
        CHECKOUT: '/api/checkout'
    };

    var _clientInstance = null;
    var _hostedFieldsInstance = null;
    var _paymentMethodNonce = null;

    //public facade 
    return {
        init: init,
        getPaymentMethodNonce: getPaymentMethodNonce,
        checkout: checkout
    };

    ///////////////////////

    function init(cardNumId, cvvId, expDateId) {
        var d = $.Deferred();

        //1. get token
        //2. create client
        //3. create hosted fields

        getClientToken()
            .done(function onOk(clientToken) {
                createClient(clientToken)
                    .done(function onOk() {
                        createHostedFields(cardNumId, cvvId, expDateId)
                            .done(function onOk() {
                                d.resolve();
                            })
                            .fail(onError);
                    })
                    .fail(onError);
            })
            .fail(onError);

        function onError(err) {
            d.reject(err);
        }

        return d.promise();
    }
    function createClient(clientToken) {
        var d = $.Deferred();

        braintree.client.create({
            authorization: clientToken
        }, function (clientErr, clientInstance) {
            if (clientErr) {
                _clientInstance = null;
                d.reject(clientErr);
            } else {
                _clientInstance = clientInstance;
                d.resolve(clientInstance);
            }
        });

        return d.promise();
    }
    function createHostedFields(cardNumId, cvvId, expDateId) {
        var d = $.Deferred();

        if (!_clientInstance) {
            d.reject(new Error('clientInstance does not exist yet.'));
        } else {

            var DEFAULT_CARD_NUMBER_ID = '#card-number';
            var DEFAULT_CVV_ID = '#cvv';
            var DEFAULT_EXPIRATION_DATE_ID = '#expiration-date';

            var hostedFieldsStyles = {
                'input': {
                    'font-size': '14pt'
                },
                'input.invalid': {
                    'color': 'red'
                },
                'input.valid': {
                    'color': 'green'
                }
            };
            var hostedFields = {
                number: {
                    selector: cardNumId || DEFAULT_CARD_NUMBER_ID,
                    placeholder: '4111 1111 1111 1111'
                },
                cvv: {
                    selector: cvvId || DEFAULT_CVV_ID,
                    placeholder: '123'
                },
                expirationDate: {
                    selector: expDateId || DEFAULT_EXPIRATION_DATE_ID,
                    placeholder: '10/2019'
                }
            };

            braintree.hostedFields.create({
                client: _clientInstance,
                styles: hostedFieldsStyles,
                fields: hostedFields
            }, function (hostedFieldsErr, hostedFieldsInstance) {
                if (hostedFieldsErr) {
                    _hostedFieldsInstance = null;
                    d.reject(hostedFieldsErr);
                } else {
                    _hostedFieldsInstance = hostedFieldsInstance;
                    d.resolve(hostedFieldsInstance);
                }
            });
        }

        return d.promise();
    }
    function getPaymentMethodNonce() {
        var d = $.Deferred();

        if (!_hostedFieldsInstance) {
            d.reject(new Error('hostedFieldsInstance does not exist yet.'));
        } else {
            _hostedFieldsInstance.tokenize(function (tokenizeErr, payload) {
                if (tokenizeErr) {
                    _paymentMethodNonce = null;
                    d.reject(tokenizeErr);
                } else {
                    _paymentMethodNonce = payload.nonce;
                    d.resolve(payload.nonce);
                }
            });
        }

        return d.promise();
    }
    function getClientToken() {
        var d = $.Deferred();

        $.get(API.CLIENT_TOKEN)
            .done(onOk)
            .fail(onError)
            .always(function () {
                //alert("finished");
            });

        function onOk(data) {
            d.resolve(data);
        }
        function onError(err) {
            d.reject(err);
        }

        return d.promise();
    }
    function checkout(amount, paymentMethodNonce) {
        var d = $.Deferred();

        var payload = {
            'amount': amount,
            'payment_method_nonce': paymentMethodNonce
        };

        $.post(API.CHECKOUT, payload)
            .done(onOk)
            .fail(onError)
            .always(function () { });

        function onOk(data) {
            if (data.success) {
                d.resolve(data);
            } else {
                d.reject(data);
            }
        }
        function onError(err) {
            d.reject(err);
        }

        return d.promise();
    }
})();